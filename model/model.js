const Employee = function (
  accountVal,
  nameVal,
  emailVal,
  passwordVal,
  dateVal,
  salaryVal,
  positionVal,
  hourVal
) {
  this.account = accountVal;
  this.name = nameVal;
  this.email = emailVal;
  this.password = passwordVal;
  this.date = dateVal;
  this.salary = salaryVal;
  this.position = positionVal;
  this.hour = hourVal;
};

Employee.prototype.calcSalary = function (salary, position) {
  if (this.position === "Sếp") return this.salary * 3;
  if (this.position === "Trưởng phòng") return this.salary * 2;
  if (this.position === "Nhân viên") return this.salary;
};

Employee.prototype.checkType = function () {
  if (this.hour >= 192) return "xuất sắc";
  if (this.hour >= 176) return "giỏi";
  if (this.hour >= 160) {
    return "khá";
  } else {
    return "trung bình";
  }
};
