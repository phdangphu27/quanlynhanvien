const checkEmpty = (userInput, idErr, message) => {
  if (!userInput) {
    showMessage(idErr, message);
    // console.log("empty", false);
    return false;
  } else if (userInput === "Chọn chức vụ") {
    showMessage(idErr, message);
    // console.log("empty", false);
    return false;
    // BUG BUG BUG
  } else {
    hideMessage(idErr);
    // console.log("empty", true);
    return true;
  }
};

const checkLength = (min, max, userInput, idErr, message) => {
  if (userInput.length >= +min && userInput.length <= +max) {
    hideMessage(idErr);
    // console.log("length", true);
    return true;
  } else {
    showMessage(idErr, message);
    // console.log("length", false);
    return false;
  }
};

const checkLetter = function (userInput, idErr) {
  const reg = /^[A-Za-z\s]*$/;
  let isLetter = reg.test(userInput);

  if (isLetter) {
    hideMessage(idErr);
    // console.log("letter", true);
    return true;
  } else {
    showMessage(idErr, "Ten khong dung dinh dang");
    // console.log("letter", false);
    return false;
  }
};

const checkEmail = function (userInput, idErr) {
  const reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  let isEmail = reg.test(userInput);

  if (isEmail) {
    hideMessage(idErr);
    // console.log("email", true);
    return true;
  } else {
    showMessage(idErr, "Email khong dung dinh dang");
    // console.log("email", false);
    return false;
  }
};

const checkNumber = function (userInput, idErr, message) {
  const reg = /\d/;

  let isNumber = reg.test(userInput);

  if (isNumber) {
    hideMessage(idErr);
    // console.log("number", true);
    return true;
  } else {
    showMessage(idErr, message);
    // console.log("number", false);
    return false;
  }
};

const checkUpperCase = function (userInput, idErr, message) {
  const reg = /[A-Z]/;

  let isUppercase = reg.test(userInput);

  if (isUppercase) {
    hideMessage(idErr);
    // console.log("uppercase", true);
    return true;
  } else {
    showMessage(idErr, message);
    // console.log("uppercase", false);
    return false;
  }
};

const checkSpecialChar = function (userInput, idErr, message) {
  const reg = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;

  let isSpecial = reg.test(userInput);

  if (isSpecial) {
    hideMessage(idErr);
    // console.log("special", true);
    return true;
  } else {
    showMessage(idErr, message);
    // console.log("special", false);
    return false;
  }
};

const checkRange = function (min, max, userInput, idErr, message) {
  if (+min <= +userInput && +userInput <= +max) {
    hideMessage(idErr);
    // console.log("range", true);
    return true;
  } else {
    showMessage(idErr, message);
    // console.log("range", false);
    return false;
  }
};

const checkDate = function (userInput, idErr) {
  const reg =
    /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$/;

  let isDate = reg.test(userInput);

  if (isDate) {
    hideMessage(idErr);

    // console.log(true);
    return true;
  } else {
    showMessage(idErr, "Ngay khong dung dinh dang");

    // console.log(false);
    return false;
  }
};

const checkRepeated = function (userInput, idErr) {
  const i = index(userInput, arr);

  if (i === -1) {
    hideMessage(idErr);
    return true;
  } else {
    showMessage(idErr, "Tai khoan khong duoc trung");
    return false;
  }
};

const checkValid = function (emObj) {
  const account =
    checkEmpty(emObj.account, "tbTKNV", "Tai khoan khong duoc de trong") &&
    checkLength(4, 6, emObj.account, "tbTKNV", "Tai khoan khong dung do dai");

  // NAME
  const name =
    checkEmpty(emObj.name, "tbTen", "Ho va ten khong duoc de trong") &&
    checkLetter(emObj.name, "tbTen");

  // EMAIL
  const email =
    checkEmpty(emObj.email, "tbEmail", "Email khong duoc de trong") &&
    checkEmail(emObj.email, "tbEmail");

  // PASSWORD
  const password =
    checkEmpty(emObj.password, "tbMatKhau", "Mat khau khong duoc de trong") &&
    checkLength(
      6,
      10,
      emObj.password,
      "tbMatKhau",
      "Mat khau khong dung do dai"
    ) &&
    checkNumber(
      emObj.password,
      "tbMatKhau",
      "Mat khau phai chua toi thieu 1 ky tu so"
    ) &&
    checkUpperCase(
      emObj.password,
      "tbMatKhau",
      "Mat khau phai chua it nhat 1 ky tu in hoa"
    ) &&
    checkSpecialChar(
      emObj.password,
      "tbMatKhau",
      "Mat khau phai chua toi thieu 1 ky tu dac biet"
    );

  // DATE
  const date =
    checkEmpty(emObj.date, "tbNgay", "Ngay lam khong duoc de trong") &&
    checkDate(emObj.date, "tbNgay");

  // SALARY
  const salary =
    checkEmpty(emObj.salary, "tbLuongCB", "Luong khong duoc de trong") &&
    checkNumber(emObj.salary, "tbLuongCB", "Luong co ban khong hop le") &&
    checkRange(
      1000000,
      20000000,
      emObj.salary,
      "tbLuongCB",
      "Vui long nhap so tu 1000000 den 20000000"
    );

  // POSITION
  const position = checkEmpty(
    emObj.position,
    "tbChucVu",
    "Chuc vu khong duoc de trong"
  );

  // HOUR
  const hour =
    checkEmpty(emObj.hour, "tbGiolam", "Gio lam khong duoc de trong") &&
    checkNumber(emObj.hour, "tbGiolam", "Gio lam khong hop le") &&
    checkRange(
      80,
      200,
      emObj.hour,
      "tbGiolam",
      "Vui long nhap so tu 80 den 200"
    );

  const isValid =
    account && name && email && password && salary && hour && position && date;

  return isValid;
};
