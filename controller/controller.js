const accountEl = document.getElementById("tknv");
const nameEl = document.getElementById("name");
const emailEl = document.getElementById("email");
const passwordEl = document.getElementById("password");
const dateEl = document.getElementById("datepicker");
const salaryEl = document.getElementById("luongCB");
const positionEl = document.getElementById("chucvu");
const hourEl = document.getElementById("gioLam");
const searchEl = document.getElementById("searchName");

const formInput = function () {
  const accountVal = accountEl.value;
  const nameVal = nameEl.value;
  const passwordVal = passwordEl.value;
  const emailVal = emailEl.value;
  const dateVal = dateEl.value;
  const salaryVal = salaryEl.value;
  const positionVal = positionEl.value;
  const hourVal = hourEl.value;

  const employee = new Employee(
    accountVal,
    nameVal,
    emailVal,
    passwordVal,
    dateVal,
    salaryVal,
    positionVal,
    hourVal
  );

  return employee;
};

const renderEmployee = function (emArr) {
  let contentHTML = "";

  emArr.forEach((em) => {
    contentHTML += `
    <tr>
      <td>${em.account}</td>
      <td>${em.name}</td>
      <td>${em.email}</td>
      <td>${em.date}</td>
      <td>${em.position}</td>
      <td>${em.calcSalary(em.salary, em.position)}</td>
      <td>${"Nhân viên " + em.checkType()}</td>
      <td>
      <button class="btn btn-danger" onClick="deleteBtn('${
        em.account
      }')">Xóa</button>
      <button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onClick="modifyBtn('${
        em.account
      }')">Sửa</button>
      </td>
    </tr>`;
  });

  document.getElementById("tableDanhSach").innerHTML = contentHTML;
};

const index = function (account, emArr) {
  for (let i = 0; i < emArr.length; i++) {
    const item = emArr[i];
    if (item.account === account) {
      return i;
    }
  }
  return -1;
};

const showInputOnForm = function (emArr) {
  accountEl.value = emArr.account;
  nameEl.value = emArr.name;
  passwordEl.value = emArr.password;
  emailEl.value = emArr.email;
  dateEl.value = emArr.date;
  salaryEl.value = emArr.salary;
  positionEl.value = emArr.position;
  hourEl.value = emArr.hour;
};

const resetForm = function () {
  document.getElementById("QLNV").reset();
};

const showMessage = function (id, message) {
  document.getElementById(id).innerHTML = message;
  document.getElementById(id).style.display = "inline";
};

const hideMessage = function (id) {
  document.getElementById(id).style.display = "none";
};

const resetMessage = function () {
  const list = document.querySelectorAll(".sp-thongbao");
  list.forEach((noti) => {
    noti.style.display = "none";
  });
};
