const arr = [];

const addLocalStorage = function () {
  const newEmployeeJson = JSON.stringify(arr);
  localStorage.setItem("newEmployeeJson", newEmployeeJson);
};

const dataJson = localStorage.getItem("newEmployeeJson");

if (dataJson !== null) {
  const initialArr = JSON.parse(dataJson);

  initialArr.forEach((employee) => {
    const initialEmployee = new Employee(
      employee.account,
      employee.name,
      employee.email,
      employee.password,
      employee.date,
      employee.salary,
      employee.position,
      employee.hour
    );
    arr.push(initialEmployee);
  });
  renderEmployee(arr);
}

const addBtn = function () {
  const newEmployee = formInput();

  const inputIsValid = checkValid(newEmployee);

  const accountRepeat = checkRepeated(newEmployee.account, "tbTKNV");

  if (inputIsValid && accountRepeat) {
    arr.push(newEmployee);

    addLocalStorage();

    renderEmployee(arr);

    resetForm();
  }
};

const deleteBtn = function (account) {
  const arrIndex = index(account, arr);

  if (arrIndex !== -1) {
    arr.splice(arrIndex, 1);

    addLocalStorage();

    renderEmployee(arr);
  }
};

const modifyBtn = function (account) {
  const arrIndex = index(account, arr);

  if (arrIndex === -1) return;

  const data = arr[arrIndex];

  showInputOnForm(data);

  document.getElementById("tknv").disabled = true;

  document.getElementById("btnThemNV").disabled = true;
};

const update = function () {
  const toBeFixedData = formInput();
  console.log(toBeFixedData);

  const arrIndex = index(toBeFixedData.account, arr);

  if (arrIndex === -1) return;

  const isValid = checkValid(toBeFixedData);

  if (isValid) {
    arr[arrIndex] = toBeFixedData;

    addLocalStorage();

    renderEmployee(arr);

    document.getElementById("tknv").disabled = false;

    document.getElementById("btnThemNV").disabled = false;

    resetForm();
  }
  console.log(arr);

  hideMessage("tbTKNV");
};

const closeBtn = function () {
  resetForm();

  resetMessage();

  document.getElementById("tknv").disabled = false;

  document.getElementById("btnThemNV").disabled = false;
};

const findBtn = function () {
  const searchVal = searchEl.value.toLowerCase();
  const searchArr = searchVal.split(" ");

  const newArr = arr.filter((em) => {
    const typeArr = em.checkType().toLowerCase().split(" ");

    return typeArr.includes(searchArr.at(-1));
  });

  renderEmployee(newArr);

  if (!searchVal) {
    renderEmployee(arr);
  }

  if (
    searchVal === "nhân viên" ||
    searchVal === "nhân" ||
    searchVal === "viên"
  ) {
    renderEmployee(arr);
  }
};
